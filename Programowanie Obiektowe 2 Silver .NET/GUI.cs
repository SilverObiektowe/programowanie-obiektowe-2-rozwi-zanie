﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programowanie_Obiektowe_2_Silver.NET
{
    public partial class GUI : Form
    {
        private Firma firma1;
        public Firma Firma1
        {
            get { return firma1; }
            set
            {
                firma1 = value;
                WypiszPracownikowFirmyNaLiscie();
                this.Text = firma1.Nazwa;
            }
        }

        public GUI()
        {
            InitializeComponent();
        } 
        
        public void WypiszPracownikowFirmyNaLiscie()
        {
            listBoxPracownicy.Items.Clear();

            for (int i = 0; i < firma1.LiczbaPracownikow; i++)
            {
                Pracownik aktualny = firma1[i];
                listBoxPracownicy.Items.Add(aktualny.ImieINazwisko);
            }
        }

        private void buttonDodajPracownika_Click(object sender, EventArgs e)
        {
            Pracownik p = new Pracownik(textBoxImie.Text, textBoxNazwisko.Text, 
                Convert.ToInt32(textBoxWiek.Text), Convert.ToDouble(textBoxPlaca.Text), 
                textBoxStanowisko.Text);

            firma1.DodajPracownika(p);

            WypiszPracownikowFirmyNaLiscie();
        }

        private void buttonUsunPracownika_Click(object sender, EventArgs e)
        {
            Firma1.UsuńPracownika((string)listBoxPracownicy.SelectedItem);
            WypiszPracownikowFirmyNaLiscie();
        }
    }
}
