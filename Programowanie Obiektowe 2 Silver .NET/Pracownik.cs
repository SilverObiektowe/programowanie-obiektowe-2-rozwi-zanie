﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programowanie_Obiektowe_2_Silver.NET
{
    public class Pracownik
    {
        private string imie;
        private string nazwisko;
        private int wiek;
        private double placa;
        private string stanowisko;

        public string ImieINazwisko { get { return imie + " " + nazwisko; } }
        public string StanowiskoPracownika { get { return stanowisko + ImieINazwisko; } }
        public string WiekStanowisko { get { return wiek + " " + stanowisko; } }

        public string Imie { get { return imie; } set { imie = value; } }
        public string Nazwisko { get { return nazwisko; } set { nazwisko = value; } }
        public int Wiek { get { return wiek; } set { wiek = value; } }
        public double Placa { get { return placa; } set { placa = value; } }
        public string Stanowisko { get { return stanowisko; } set { stanowisko = value; } }

        public Pracownik(string imie, string nazwisko, int wiek, double placa, string stanowisko)
        {
            this.imie = imie;
            this.nazwisko = nazwisko;
            this.wiek = wiek;
            this.placa = placa;
            this.stanowisko = stanowisko;
        }
    }
}
