﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Programowanie_Obiektowe_2_Silver.NET
{
    public class Firma
    {
        private string nazwa;
        private string nip;
        private List<Pracownik> listaPracownikow;

        public string Nazwa { get { return nazwa; } set { nazwa = value; } }
        public string NIP { get { return nip; } }

        public Pracownik this[int index]
        {
            get { return listaPracownikow[index]; }
            set { listaPracownikow[index] = value; }
        }
        public int LiczbaPracownikow { get { return listaPracownikow.Count; } }

        public Firma(string nazwa, string nip, List<Pracownik> lista)
        {
            this.listaPracownikow = lista;
            this.nazwa = nazwa;
            this.nip = nip;
        }

        public void DodajPracownika(Pracownik p)
        {
            listaPracownikow.Add(p);
        }

        public void UsuńPracownika(string ImieINazwisko)
        {
            for (int i = 0; i < LiczbaPracownikow; i++)
            {
                if (listaPracownikow[i].ImieINazwisko == ImieINazwisko)
                {
                    listaPracownikow.RemoveAt(i);
                }
            }
        }
    }
}
