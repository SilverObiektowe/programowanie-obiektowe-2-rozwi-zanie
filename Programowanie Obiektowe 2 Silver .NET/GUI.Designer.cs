﻿namespace Programowanie_Obiektowe_2_Silver.NET
{
    partial class GUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listBoxPracownicy = new System.Windows.Forms.ListBox();
            this.textBoxImie = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonDodajPracownika = new System.Windows.Forms.Button();
            this.buttonUsunPracownika = new System.Windows.Forms.Button();
            this.textBoxNazwisko = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxWiek = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxPlaca = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBoxStanowisko = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // listBoxPracownicy
            // 
            this.listBoxPracownicy.FormattingEnabled = true;
            this.listBoxPracownicy.ItemHeight = 16;
            this.listBoxPracownicy.Location = new System.Drawing.Point(323, 12);
            this.listBoxPracownicy.Name = "listBoxPracownicy";
            this.listBoxPracownicy.Size = new System.Drawing.Size(237, 324);
            this.listBoxPracownicy.TabIndex = 0;
            // 
            // textBoxImie
            // 
            this.textBoxImie.Location = new System.Drawing.Point(24, 47);
            this.textBoxImie.Name = "textBoxImie";
            this.textBoxImie.Size = new System.Drawing.Size(212, 22);
            this.textBoxImie.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "Imie";
            // 
            // buttonDodajPracownika
            // 
            this.buttonDodajPracownika.Location = new System.Drawing.Point(12, 357);
            this.buttonDodajPracownika.Name = "buttonDodajPracownika";
            this.buttonDodajPracownika.Size = new System.Drawing.Size(289, 33);
            this.buttonDodajPracownika.TabIndex = 3;
            this.buttonDodajPracownika.Text = "Dodaj Pracownika";
            this.buttonDodajPracownika.UseVisualStyleBackColor = true;
            this.buttonDodajPracownika.Click += new System.EventHandler(this.buttonDodajPracownika_Click);
            // 
            // buttonUsunPracownika
            // 
            this.buttonUsunPracownika.Location = new System.Drawing.Point(323, 357);
            this.buttonUsunPracownika.Name = "buttonUsunPracownika";
            this.buttonUsunPracownika.Size = new System.Drawing.Size(237, 33);
            this.buttonUsunPracownika.TabIndex = 3;
            this.buttonUsunPracownika.Text = "Usuń zaznaczonego pracownika";
            this.buttonUsunPracownika.UseVisualStyleBackColor = true;
            this.buttonUsunPracownika.Click += new System.EventHandler(this.buttonUsunPracownika_Click);
            // 
            // textBoxNazwisko
            // 
            this.textBoxNazwisko.Location = new System.Drawing.Point(24, 92);
            this.textBoxNazwisko.Name = "textBoxNazwisko";
            this.textBoxNazwisko.Size = new System.Drawing.Size(212, 22);
            this.textBoxNazwisko.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 17);
            this.label2.TabIndex = 2;
            this.label2.Text = "Nazwisko";
            // 
            // textBoxWiek
            // 
            this.textBoxWiek.Location = new System.Drawing.Point(24, 146);
            this.textBoxWiek.Name = "textBoxWiek";
            this.textBoxWiek.Size = new System.Drawing.Size(212, 22);
            this.textBoxWiek.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Wiek";
            // 
            // textBoxPlaca
            // 
            this.textBoxPlaca.Location = new System.Drawing.Point(24, 191);
            this.textBoxPlaca.Name = "textBoxPlaca";
            this.textBoxPlaca.Size = new System.Drawing.Size(212, 22);
            this.textBoxPlaca.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 171);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 17);
            this.label4.TabIndex = 2;
            this.label4.Text = "Placa";
            // 
            // textBoxStanowisko
            // 
            this.textBoxStanowisko.Location = new System.Drawing.Point(24, 236);
            this.textBoxStanowisko.Name = "textBoxStanowisko";
            this.textBoxStanowisko.Size = new System.Drawing.Size(212, 22);
            this.textBoxStanowisko.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 2;
            this.label5.Text = "Stanowisko";
            // 
            // GUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 402);
            this.Controls.Add(this.buttonUsunPracownika);
            this.Controls.Add(this.buttonDodajPracownika);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.textBoxStanowisko);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxPlaca);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textBoxWiek);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBoxNazwisko);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBoxImie);
            this.Controls.Add(this.listBoxPracownicy);
            this.Name = "GUI";
            this.Text = "Nazwa firmy";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listBoxPracownicy;
        private System.Windows.Forms.TextBox textBoxImie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDodajPracownika;
        private System.Windows.Forms.Button buttonUsunPracownika;
        private System.Windows.Forms.TextBox textBoxNazwisko;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxWiek;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxPlaca;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox textBoxStanowisko;
        private System.Windows.Forms.Label label5;
    }
}