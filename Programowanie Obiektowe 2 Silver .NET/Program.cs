﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Programowanie_Obiektowe_2_Silver.NET
{
    class Program
    {
        // Polecenie zadania:
        // http://1drv.ms/19mCvdY

        static void Main(string[] args)
        {
            Firma firma = new Firma("Kebab", "12345", new List<Pracownik>() { 
                new Pracownik("Jan", "Kowalski", 25, 6000, "architekt"),
                new Pracownik("Pawel", "Nowak", 30, 7000, "Programista"),
            });

            GUI gui = new GUI();
            gui.Firma1 = firma;
            Application.Run(gui);
        }
    }
}
